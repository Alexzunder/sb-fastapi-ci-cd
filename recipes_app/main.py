import logging
from logging.config import dictConfig
from typing import List, Optional

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload

from recipes_app import models, schemas
from recipes_app.config import LogConfig
from recipes_app.database import async_session, engine

dictConfig(LogConfig().dict())
logger = logging.getLogger('cookbook_api')


app = FastAPI()


async def get_db():
    async with async_session() as session:
        yield session


@app.on_event('startup')
async def startup():
    logger.debug('start server')
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event('shutdown')
async def shutdown(session=Depends(get_db)):
    logger.debug('shutdown')
    await session.close()
    await engine.dispose()


@app.get('/recipe/{recipe_id}', response_model=schemas.RecipeOut)
async def recipes(recipe_id: int, session=Depends(get_db)) -> Optional[schemas.RecipeOut]:
    async with session.begin():
        these_recipes = await session.execute(
            select(models.Recipe).where(models.Recipe.id == recipe_id).options(
                selectinload(models.Recipe.ingredients),
            ),
        )
        recipe = these_recipes.scalar_one_or_none()
        if recipe is None:
            raise HTTPException(status_code=404, detail='Recipe not found')
        recipe.views_number += 1
        await session.commit()
        return schemas.RecipeOut(
            id=recipe_id,
            dish_name=recipe.dish_name,
            cooking_time=recipe.cooking_time,
            ingredients=[ingredient.name for ingredient in recipe.ingredients],
            description=recipe.description,
        )


@app.get('/titles', response_model=List[schemas.RecipeBrief])
async def get_titles(session=Depends(get_db)) -> List[schemas.RecipeBrief]:
    these_titles = await session.execute(
        select(models.Recipe).order_by(
            models.Recipe.views_number.desc(), models.Recipe.cooking_time.desc(),
        ),
    )
    titles = these_titles.scalars().all()
    logger.debug(titles)
    return [
        schemas.RecipeBrief(
            dish_name=title.dish_name,
            views_number=title.views_number,
            cooking_time=title.cooking_time,
        ) for title in titles
    ]
