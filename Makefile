run:
	uvicorn recipes_app.main:app --reload

tests:
	python -m pytest

lint:
	@flake8 recipes_app
	@mypy recipes_app
